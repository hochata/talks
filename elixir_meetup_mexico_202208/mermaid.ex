defmodule Mermaid.Flowchart do
  defmodule Node do
    @shapes %{
      rect: {"[", "]"},
      round: {"(", ")"},
      stad: {"([", "])"},
      d_rect: {"[[", "]]"},
      cyl: {"[(", ")]"},
      circ: {"((", "))"},
      asym: {">", "]"},
      rhomb: {"{", "}"},
      hex: {"{{", "}}"},
      par: {"[/", "/]"},
      par_alt: {"[\\", "\\]"},
      trap: {"[/", "\\]"},
      trap_alt: {"[\\", "/]"},
      d_circ: {"(((", ")))"}
    }

    @shape_atoms Map.keys(@shapes)

    def shape?(atom), do: atom in @shape_atoms

    defstruct [:id, :text, :shape]

    def new(id, opts \\ []) do
      text = Keyword.get(opts, :text)
      shape = Keyword.get(opts, :shape, :rect)
      %__MODULE__{id: id, text: text, shape: shape}
    end

    def to_string(%__MODULE__{id: id, text: text, shape: shape}) do
      {left, right} = Map.get(@shapes, shape)

      if is_nil(text) do
        Atom.to_string(id)
      else
        IO.iodata_to_binary([Atom.to_string(id), left, text, right])
      end
    end

    defimpl String.Chars do
      def to_string(t), do: Mermaid.Flowchart.Node.to_string(t)
    end
  end

  defmodule Link do
    @direction [:none, :in, :out, :both]
    @weight %{default: "-", dot: ".", thick: "="}
    @head %{arrow: ">", o: "o", x: "x"}

    defstruct direction: :none, weight: :default, head: :arrow, lenght: 1, text: nil

    def arrow?(arrow), do: arrow in [:arrow, :o, :x]

    def weight?(w), do: w in [:dot, :thick]

    def direction?(d), do: d in @direction

    def new(opts \\ []) do
      struct(__MODULE__, Map.new(opts))
    end

    defp get_arrows(">"), do: {"<", ">"}
    defp get_arrows(other), do: {other, other}

    defp get_border("."), do: "-"
    defp get_border(other), do: other

    defp maybe_pad({left, right}, "."), do: {left <> "-", "-" <> right}
    defp maybe_pad(other, _weight), do: other

    def to_string(%__MODULE__{} = link) do
      segment = Map.get(@weight, link.weight)
      {left, right} = Map.get(@head, link.head) |> get_arrows() |> maybe_pad(segment)
      border = get_border(segment)
      interior = String.duplicate(segment, link.lenght)

      str_link =
        case link.direction do
          :none -> [border, interior, border]
          :in -> [left, interior, border]
          :out -> [border, interior, right]
          :both -> [left, interior, right]
        end

      if is_nil(link.text) do
        str_link
      else
        [str_link, "|", link.text, "|"]
      end
      |> IO.iodata_to_binary()
    end

    defimpl String.Chars do
      def to_string(t), do: Mermaid.Flowchart.Link.to_string(t)
    end
  end

  require EEx

  @orientations [:tb, :td, :bt, :rl, :lr]

  defstruct orientation: nil, decls: []

  def new(orient, decls \\ []) when orient in @orientations do
    %__MODULE__{orientation: orient, decls: decls}
  end

  def add(%__MODULE__{} = f, l) when is_list(l) do
    %{f | decls: [l | f.decls]}
  end

  @mermaid_template """
  flowchart <%= flow.orientation%> <%= for line <- flow.decls do %>
    <%= Enum.intersperse(line, " ") %><% end %>
  """

  EEx.function_from_string(:defp, :embed_mermaid, @mermaid_template, [:flow])

  defp serialize(%__MODULE__{} = f) do
    %__MODULE__{
      orientation: f.orientation |> Atom.to_string() |> String.upcase(),
      decls: Enum.map(f.decls, fn line -> Enum.map(line, &Kernel.to_string/1) end)
    }
  end

  def to_string(%__MODULE__{} = f) do
    f
    |> serialize()
    |> embed_mermaid()
  end

  defimpl String.Chars do
    def to_string(t), do: Mermaid.Flowchart.to_string(t)
  end

  defmacro flowchart(orientation, do: {:__block__, _meta, lines}) do
    build(orientation, lines)
  end

  defmacro flowchart(orientation, do: lines) do
    build(orientation, lines)
  end

  defp build(orientation, lines) do
    decls =
      lines
      |> flatten_lines()
      |> chunk_lines()
      |> Enum.map(fn line -> Enum.map(line, &build_entity/1) end)

    new(orientation, decls)|> Macro.escape()
  end

  @link_symbs [:<~, :--, :<~>, :~>]

  defp flatten_lines(lines) when is_list(lines),
    do: Enum.map(lines, &flatten_line/1)

  defp flatten_lines(line), do: [flatten_line(line)]

  defp flatten_line(terms) when is_list(terms),
    do: Enum.map(terms, &flatten_line/1)

  defp flatten_line({op, _meta, module}) when is_atom(module), do: {:id, op}

  defp flatten_line({op, _meta, [left, right]}) when op in @link_symbs,
    do: List.flatten([flatten_line(left), op, flatten_line(right)])

  defp flatten_line({:{}, _, args}), do: args

  defp flatten_line({a0, a1}), do: [a0, a1]

  defp flatten_line({op, _meta, args}) when is_list(args),
    do: [{:id, op} | flatten_lines(args)]

  defp flatten_line(other), do: other

  defp chunk_lines([l | _] = lines) when is_list(l),
    do: Enum.map(lines, &chunk_line(&1))

  defp chunk_lines(line), do: chunk_line(line)

  defp chunk_line(line) do
    Enum.chunk_while(
      line,
      [],
      &chunk_by_elem/2,
      &{:cont, format_chunk(&1), []}
    ) |> Enum.filter(&(not Enum.empty?(&1)))
  end

  defp chunk_by_elem({:id, _} = id, acc),
    do: {:cont, format_chunk(acc), [id]}

  defp chunk_by_elem(arrow, acc) when arrow in @link_symbs do
    if Enum.any?(acc, &(&1 in @link_symbs)) do
      {:cont, format_chunk([arrow | acc]), []}
    else
      {:cont, format_chunk(acc), [arrow]}
    end
  end

  defp chunk_by_elem(other, acc), do: {:cont, [other | acc]}

  defp format_chunk(chunk), do: chunk |> Enum.reverse()

  defp build_entity([{:id, id} | opts]) do
    text = Enum.find(opts, &is_bitstring/1)
    shape = Enum.find(opts, :rect, &Node.shape?/1)
    Node.new(id, text: text, shape: shape)
  end

  defp build_entity([l_arr | link_opts]) do
    text = Enum.find(link_opts, &is_bitstring/1)
    lenght = Enum.find(link_opts, 1, &is_integer/1)
    arrow = Enum.find(link_opts, :arrow, &Link.arrow?/1)
    weight = Enum.find(link_opts, :default, &Link.weight?/1)
    dir = get_direction(l_arr, List.last(link_opts))

    Link.new(
      text: text,
      lenght: lenght,
      head: arrow,
      weight: weight,
      direction: dir
    )
  end

  defp get_direction(:<~, :~>), do: :both
  defp get_direction(:<~>, _), do: :both
  defp get_direction(:~>, _), do: :out
  defp get_direction(:<~, _), do: :in
  defp get_direction(:--, :~>), do: :out
  defp get_direction(:--, _), do: :none
end
